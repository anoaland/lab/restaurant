export interface Restaurant {
  id: string
  name: string
  address: string
  city: string
}

export interface RestaurantDetail extends Restaurant {
  reviews: RestaurantReview[]
}

export interface RestaurantReview {
  id: string
  text: string
  ratingColor: string
  user: string
}