import { Restaurant, RestaurantDetail } from './restaurant'

export interface MainScreenState {
  query: string
  restaurants: Restaurant[]
  viewMode: 'list' | 'detail'
  selectedRestaurant?: Restaurant
  restaurantDetail?: RestaurantDetail
  loading: boolean
}
