import axios from 'axios'
import React from 'react'
import {
  ActivityIndicator,
  Button,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import { MainScreenProps } from './props'
import { Restaurant, RestaurantReview } from './restaurant'
import { MainScreenState } from './state'

const API_KEY = '6c61897d907f2ba873e7883f6c7a3aa8'
const API_URL = 'https://developers.zomato.com/api/v2.1/'

export class MainScreen extends React.Component<
  MainScreenProps,
  MainScreenState
> {
  constructor(props: MainScreenProps) {
    super(props)
    this.state = {
      query: '',
      restaurants: [],
      viewMode: 'list',
      loading: false
    }
    this.handleSearch = this.handleSearch.bind(this)
    this.handleDetail = this.handleDetail.bind(this)
  }

  public render() {
    const { loading } = this.state
    return (
      <View style={[styles.container, { marginTop: StatusBar.currentHeight }]}>
        {this.state.viewMode === 'list'
          ? this.renderList()
          : this.renderDetail()}
        {loading && <ActivityIndicator size="large" color="#0000ff" />}
      </View>
    )
  }

  public renderList() {
    const { query, restaurants } = this.state
    return (
      <View>
        <View>
          <TextInput
            style={styles.text}
            value={query}
            onChangeText={t => this.setState({ query: t })}
          />
          <Button title="Cari" onPress={this.handleSearch} />
          <ScrollView>
            {restaurants.map(r => (
              <TouchableOpacity
                key={r.id}
                style={styles.item}
                onPress={() => this.handleDetail(r)}
              >
                <Text style={styles.name}>{r.name}</Text>
                <Text>{r.address}</Text>
                <Text>{r.city}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
    )
  }

  public renderDetail() {
    const { restaurantDetail } = this.state
    if (!restaurantDetail) {
      return null
    }

    return (
      <View style={styles.item}>
        <Text style={styles.name}>{restaurantDetail.name}</Text>
        <Text>{restaurantDetail.address}</Text>
        <Text>{restaurantDetail.city}</Text>
        <View>
          <Text>REVIEWS:</Text>
          {restaurantDetail.reviews.length ? (
            <View>
              {restaurantDetail.reviews.map(r => (
                <View>
                  <Text style={{ fontWeight: '600', color: r.ratingColor }}>
                    {r.text}
                  </Text>
                  <Text>{r.user}</Text>
                </View>
              ))}
            </View>
          ) : (
            <Text>No review yet!</Text>
          )}
        </View>
        <Button
          title="KEMBALI"
          onPress={() => {
            this.setState({ viewMode: 'list' })
          }}
        />
      </View>
    )
  }

  handleSearch() {
    const { query } = this.state
    this.setState({ loading: true })
    axios
      .get(`${API_URL}search?q=${query}&count=10`, {
        headers: {
          'user-key': API_KEY
        }
      })
      .then(val => {
        if (val.data && val.data.restaurants) {
          const restaurants = (val.data.restaurants as any[])
            .map(r => r.restaurant)
            .map<Restaurant>(r => ({
              id: r.id,
              name: r.name,
              address: r.location.address,
              city: r.location.city
            }))
          this.setState({ restaurants, loading: false })
        }
      })
  }

  handleDetail(restaurant: Restaurant) {
    this.setState({ loading: true })
    axios
      .get(`${API_URL}restaurant?res_id=${restaurant.id}`, {
        headers: {
          'user-key': API_KEY
        }
      })
      .then(val => {
        if (val.data && val.data.all_reviews) {
          const reviews = (val.data.all_reviews as any[]).map<RestaurantReview>(
            r => ({
              id: r.id,
              ratingColor: r.rating_color,
              text: r.review_text,
              user: r.user.name
            })
          )
          this.setState({
            viewMode: 'detail',
            selectedRestaurant: restaurant,
            restaurantDetail: {
              ...restaurant,
              reviews
            },
            loading: false
          })
        } else {
          this.setState({
            viewMode: 'detail',
            selectedRestaurant: restaurant,
            restaurantDetail: {
              ...restaurant,
              reviews: []
            },
            loading: false
          })
        }
      })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    padding: 10
  },
  item: {
    padding: 24
  },
  name: {
    fontWeight: '600'
  }
})
